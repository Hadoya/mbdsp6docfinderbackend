const router=require('express').Router()
let Speciality=require('../models/speciality.model') 

router.route('').get((req,res)=>{
     Speciality.find()
     .then(speciality=>res.json(speciality))
     .catch(err=>res.status(400).json('Error:' +err));
});

router.route('/add').post((req,res)=>{
    const name=req.body.name
    const description=req.body.description
  
    
    const newSpeciality=new Speciality({
      name,
       description,
 
    })
    
    newSpeciality.save()
    .then(speciality=>res.json('Speciality added'))
    .catch(err=>res.status(400).json('Error:' +err));

})
router.route('/:id').get((req,res)=>{
    Speciality.findById(req.params.id)
    .then(speciality=>res.json(speciality))
    .catch(err=>res.status(400).json('Error:'+err))
})
router.route('/update/:id').post((req,res)=>{
    Speciality.findById(req.params.id)
    .then(speciality=>{
        speciality.name=req.body.name
          speciality.description=req.body.description
        

        speciality.save()
            .then(()=>res.json('Speciality updated'))
            .catch(err=>res.status(400).json('Error:' +err));
    })
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/:id').delete((req,res)=>{
    Speciality.findByIdAndDelete(req.params.id)
    .then(()=>res.json('Speciality deleted'))
    .catch(err=>res.status(400).json('Error:'+err))
})


module.exports=router;