const router=require('express').Router()
let UserAdmin=require('../models/userAdmin.model') 

router.route('/').get((req,res)=>{
    UserAdmin.find().sort({createdAt:-1})
     .then(userAdmin=>res.json(userAdmin))
     .catch(err=>res.status(400).json('Error:' +err));
});

router.route('/add').post((req,res)=>{
    const lastname=req.body.lastname;
    const firstname=req.body.firstname;
    const username=req.body.username;
    const email=req.body.email;
    const password=req.body.password;
    
    
    const newUserAdmin=new UserAdmin({
        lastname,
        firstname,
        username,
        email,
        password
    })
    
    newUserAdmin.save()
    .then(userAdmin=>res.json('UserAdmin added'))
    .catch(err=>res.status(400).json('Error:' +err));
})

router.route('/:id').get((req,res)=>{
    UserAdmin.findById(req.params.id)
    .then(userAdmin=>res.json(userAdmin))
    .catch(err=>res.status(400).json('Error:'+err))
})
router.route('/update/:id').post((req,res)=>{
    UserAdmin.findById(req.params.id)
    .then(userAdmin=>{
        userAdmin.lastname=req.body.lastname;
        userAdmin.firstname=req.body.firstname;
        userAdmin.username=req.body.username;
        userAdmin.email=req.body.email;
        userAdmin.password=req.body.password;
       

        userAdmin.save()
            .then(()=>res.json('UserAdmin updated'))
            .catch(err=>res.status(400).json('Error:' +err));
    })
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/:id').delete((req,res)=>{
    UserAdmin.findByIdAndDelete(req.params.id)
    .then(()=>res.json('UserAdmin deleted'))
    .catch(err=>res.status(400).json('Error:'+err))
})


router.route('/search').post((req,res)=>{
    const regex = new RegExp(req.body.wordToSearch, 'i');  // 'i' makes it case insensitive
     UserAdmin.find({$or: [{ lastname: regex }, { firstname: regex }, { username: regex }]} )
     .then(usr=>res.json(usr))
     .catch(err=>res.status(400).json('Error:' +err));
    
});

router.route('/login').post((req,res)=>{
   
    const username=req.body.username;
    const password=req.body.password;
    
    UserAdmin.findOne({username:username})
    .then(userAdmin=>{
        if(userAdmin){
            if(password==userAdmin.password){
               const user={
                   _id:userAdmin._id,
                   lastname:userAdmin.lastname,
                   firstname:userAdmin.firstname,
                   username:userAdmin.username,
                   email:userAdmin.email,

               } 
               res.send(user);
            }
            else{
                res.json({msg:"Nom d'utilisateur ou mot de passe invalide."})
            }
        }
        else{
            res.json({msg:"Utilisateur non trouvé"})
        }
    })
    .catch(err=>res.send('Error:'+err))

});




module.exports=router;