const router=require('express').Router()
let SymptomPerDisease=require('../models/symptomPerDisease.model') 

router.route('').get((req,res)=>{
     SymptomPerDisease.find()
     .then(symptomPerDisease=>res.json(symptomPerDisease))
     .catch(err=>res.status(400).json('Error:' +err));
});

router.route('/add').post((req,res)=>{
    const idDisease=req.body.idDisease
    const idSymptom=req.body.idSymptom
    const begininterval=Number(req.body.begininterval)
    const endinterval=Number(req.body.endinterval)
    const unit=req.body.unit
  
    
    const newSymptomPerDisease=new SymptomPerDisease({
        idDisease,
        idSymptom,
       begininterval,
       endinterval,
       unit,
     
    })
    
    newSymptomPerDisease.save()
    .then(symptomPerDisease=>res.json('SymptomPerDisease added'))
    .catch(err=>res.status(400).json('Error:' +err));

})
router.route('/:id').get((req,res)=>{
    SymptomPerDisease.findById(req.params.id)
    .then(symptomPerDisease=>res.json(symptomPerDisease))
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/findbyiddisease/:id').get((req,res)=>{
    SymptomPerDisease.find({idDisease:req.params.id})
    .then(symptomPerDisease=>res.json(symptomPerDisease))
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/update/:id').post((req,res)=>{
    SymptomPerDisease.findById(req.params.id)
    .then(symptomPerDisease=>{
        symptomPerDisease.idDisease=req.body.idDisease
        symptomPerDisease.idSymptom=req.body.idSymptom
         symptomPerDisease.begininterval=Number(req.body.begininterval)
          symptomPerDisease.endinterval=Number(req.body.endinterval)
          symptomPerDisease.unit=req.body.unit
        

        symptomPerDisease.save()
            .then(()=>res.json('SymptomPerDisease updated'))
            .catch(err=>res.status(400).json('Error:' +err));
    })
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/delete/:id').delete((req,res)=>{
    SymptomPerDisease.findByIdAndDelete(req.params.id)
    .then(()=>res.json('SymptomPerDisease deleted'))
    .catch(err=>res.status(400).json('Error:'+err))
})
router.route('/search').post((req,res)=>{
    const regex = new RegExp(req.body.idDisease, 'i');  // 'i' makes it case insensitive
    SymptomPerDisease.find({idDisease: regex})
     .then(symptomPerDisease=>res.json(symptomPerDisease))
     .catch(err=>res.status(400).json('Error:' +err));
    
});
router.route('/searchIdSymptom').post((req,res)=>{
    const regex = new RegExp(req.body.idSymptom, 'i');  // 'i' makes it case insensitive
    SymptomPerDisease.find({idSymptom: regex})
     .then(symptomPerDisease=>res.json(symptomPerDisease))
     .catch(err=>res.status(400).json('Error:' +err));
    
});

module.exports=router;