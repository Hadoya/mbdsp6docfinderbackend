const router=require('express').Router()
let PatientDetails=require('../models/patientDetails.model') 

router.route('').get((req,res)=>{
     PatientDetails.find()
     .then(patientDetails=>res.json(patientDetails))
     .catch(err=>res.status(400).json('Error:' +err));
});

router.route('/add').post((req,res)=>{
    const birthdate=Date.parse(req.body.birthdate)
    const height=Number(req.body.height)
    const weight=Number(req.body.weight)
    const description=req.body.description
  
    
    const newPatientDetails=new PatientDetails({
       birthdate,
       height,
       weight,
       description,
 
    })
    
    newPatientDetails.save()
    .then(patientDetails=>res.json('PatientDetails added'))
    .catch(err=>res.status(400).json('Error:' +err));

})
router.route('/:id').get((req,res)=>{
    PatientDetails.findById(req.params.id)
    .then(patientDetails=>res.json(patientDetails))
    .catch(err=>res.status(400).json('Error:'+err))
})
router.route('/update/:id').post((req,res)=>{
    PatientDetails.findById(req.params.id)
    .then(patientDetails=>{
         patientDetails.birthdate=Date.parse(req.body.birthdate)
         patientDetails.height=Number(req.body.height)
          patientDetails.weight=Number(req.body.weight)
          patientDetails.description=req.body.description
        

        patientDetails.save()
            .then(()=>res.json('PatientDetails updated'))
            .catch(err=>res.status(400).json('Error:' +err));
    })
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/:id').delete((req,res)=>{
    PatientDetails.findByIdAndDelete(req.params.id)
    .then(()=>res.json('PatientDetails deleted'))
    .catch(err=>res.status(400).json('Error:'+err))
})


module.exports=router;