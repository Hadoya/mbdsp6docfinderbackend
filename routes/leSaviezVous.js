const router=require('express').Router()
let LeSaviezVous=require('../models/leSaviezVous.model') 

router.route('').get((req,res)=>{
     LeSaviezVous.find()
     .then(leSaviezVous=>res.json(leSaviezVous))
     .catch(err=>res.status(400).json('Error:' +err));
});

router.route('/list').get((req,res)=>{
    LeSaviezVous.find().sort({createdAt:-1}).limit(50)
    .then(leSaviezVous=>res.json(leSaviezVous))
    .catch(err=>res.status(400).json('Error:' +err));
})
router.route('/add').post((req,res)=>{
    const title=req.body.title
    const idCategory=req.body.idCategory
    const image=req.body.image
    const description=req.body.description
    const insertdate=Date.parse(req.body.insertdate)
  
    
    const newLeSaviezVous=new LeSaviezVous({
        title,
        idCategory,
        image ,
        description,
        insertdate,
 
    })
    
    newLeSaviezVous.save()
    .then(leSaviezVous=>res.json('LeSaviezVous added'))
    .catch(err=>res.status(400).json('Error:' +err));

})
router.route('/:id').get((req,res)=>{
    LeSaviezVous.findById(req.params.id)
    .then(leSaviezVous=>res.json(leSaviezVous))
    .catch(err=>res.status(400).json('Error:'+err))
})
router.route('/update/:id').post((req,res)=>{
    LeSaviezVous.findById(req.params.id)
    .then(leSaviezVous=>{
        leSaviezVous.title=req.body.title
        leSaviezVous.idCategory=req.body.idCategory
        leSaviezVous.image=req.body.image
        leSaviezVous.description=req.body.description
        leSaviezVous.insertdate=Date.parse(req.body.insertdate)

        leSaviezVous.save()
            .then(()=>res.json('LeSaviezVous updated'))
            .catch(err=>res.status(400).json('Error:' +err));
    })
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/:id').delete((req,res)=>{
    LeSaviezVous.findByIdAndDelete(req.params.id)
    .then(()=>res.json('LeSaviezVous deleted'))
    .catch(err=>res.status(400).json('Error:'+err))
})
router.route('/search').post((req,res)=>{
    const regex = new RegExp(req.body.description, 'i');  // 'i' makes it case insensitive
     LeSaviezVous.find({description: regex})
     .then(lsv=>res.json(lsv))
     .catch(err=>res.status(400).json('Error:' +err));
    
});

router.route('/searchfromadmin').post((req,res)=>{
    const regex = new RegExp(req.body.wordToSearch, 'i');  // 'i' makes it case insensitive
    LeSaviezVous.find({$or: [{ title: regex }, { description: regex }]} )
     .then(lsv=>res.json(lsv))
     .catch(err=>res.status(400).json('Error:' +err));
});
module.exports=router;