const router=require('express').Router()
let Prescription=require('../models/prescription.model') 

router.route('').get((req,res)=>{
     Prescription.find()
     .then(prescription=>res.json(prescription))
     .catch(err=>res.status(400).json('Error:' +err));
});

router.route('/add').post((req,res)=>{
   const description=req.body.description
  
    
    const newPrescription=new Prescription({
       
       description,
 
    })
    
    newPrescription.save()
    .then(prescription=>res.json('Prescription added'))
    .catch(err=>res.status(400).json('Error:' +err));

})
router.route('/:id').get((req,res)=>{
    Prescription.findById(req.params.id)
    .then(prescription=>res.json(prescription))
    .catch(err=>res.status(400).json('Error:'+err))
})
router.route('/update/:id').post((req,res)=>{
    Prescription.findById(req.params.id)
    .then(prescription=>{
        
          prescription.description=req.body.description
        

        prescription.save()
            .then(()=>res.json('Prescription updated'))
            .catch(err=>res.status(400).json('Error:' +err));
    })
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/:id').delete((req,res)=>{
    Prescription.findByIdAndDelete(req.params.id)
    .then(()=>res.json('Prescription deleted'))
    .catch(err=>res.status(400).json('Error:'+err))
})


module.exports=router;