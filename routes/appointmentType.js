const router=require('express').Router()
let AppointmentType=require('../models/appointmentType.model') 

router.route('').get((req,res)=>{
     AppointmentType.find()
     .then(appointmentType=>res.json(appointmentType))
     .catch(err=>res.status(400).json('Error:' +err));
});

router.route('/add').post((req,res)=>{
    const libelle=req.body.libelle
  
    
    const newAppointmentType=new AppointmentType({
        libelle,
       
    })
    
    newAppointmentType.save()
    .then(appointmentType=>res.json('appointmentType added'))
    .catch(err=>res.status(400).json('Error:' +err));

})
router.route('/:id').get((req,res)=>{
    AppointmentType.findById(req.params.id)
    .then(appointmentType=>res.json(appointmentType))
    .catch(err=>res.status(400).json('Error:'+err))
})
router.route('/update/:id').post((req,res)=>{
    AppointmentType.findById(req.params.id)
    .then(appointmentType=>{
         appointment.libelle=req.body.libelle
     

        appointmentType.save()
            .then(()=>res.json('Appointment updated'))
            .catch(err=>res.status(400).json('Error:' +err));
    })
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/:id').delete((req,res)=>{
    AppointmentType.findByIdAndDelete(req.params.id)
    .then(()=>res.json('AppointmentType deleted'))
    .catch(err=>res.status(400).json('Error:'+err))
})


module.exports=router;