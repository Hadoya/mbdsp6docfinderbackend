const router=require('express').Router()
let DoctorSpeciality=require('../models/doctorSpeciality.model') 

router.route('').get((req,res)=>{
     DoctorSpeciality.find()
     .then(doctorSpeciality=>res.json(doctorSpeciality))
     .catch(err=>res.status(400).json('Error:' +err));
});

router.route('/add').post((req,res)=>{
    const name=req.body.name
    const description=req.body.description
  
    
    const newDoctorSpeciality=new DoctorSpeciality({
      name,
       description,
 
    })
    
    newDoctorSpeciality.save()
    .then(doctorSpeciality=>res.json('DoctorSpeciality added'))
    .catch(err=>res.status(400).json('Error:' +err));

})
router.route('/:id').get((req,res)=>{
    DoctorSpeciality.findById(req.params.id)
    .then(doctorSpeciality=>res.json(doctorSpeciality))
    .catch(err=>res.status(400).json('Error:'+err))
})
router.route('/update/:id').post((req,res)=>{
    DoctorSpeciality.findById(req.params.id)
    .then(doctorSpeciality=>{
        doctorSpeciality.name=req.body.name
          doctorSpeciality.description=req.body.description
        

        doctorSpeciality.save()
            .then(()=>res.json('DoctorSpeciality updated'))
            .catch(err=>res.status(400).json('Error:' +err));
    })
    .catch(err=>res.status(400).json('Error:'+err))
})

router.route('/delete/:id').delete((req,res)=>{
    DoctorSpeciality.findByIdAndDelete(req.params.id)
    .then(()=>res.json('DoctorSpeciality deleted'))
    .catch(err=>res.status(400).json('Error:'+err))
})


module.exports=router;