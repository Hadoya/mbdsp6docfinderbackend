const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userAdminSchema = new Schema(
    {
        lastname: { type: String, required: true },
        firstname: { type: String, required: false },
        username: { type: String, required: true },
        email: { type: String, required: true },
        password: { type: String, required: true },
    },
    { 
        timestamps: true 
    },
)

module.exports = mongoose.model('UserAdmin', userAdminSchema);