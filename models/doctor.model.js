const mongoose = require('mongoose')
const Schema = mongoose.Schema

const doctorShema = new Schema(
    {
        lastname: { type: String},
        firstname: { type: String},
        idDoctorSpeciality: { type: String},
        speciality:{type:String, default:'généraliste'},
        telephone: { type: String},
        address: { type: String},
        town: { type: String},
        city: { type: String},
        description: { type: String},
        tarif: { type: String},
        etatValidation:{type: Number},
        password: { type: String },
        email: { type: String },
        gender:{type:String, default:"m"},
        idPatient:{ type: String }
       
    },
    { timestamps: true },
)

module.exports = mongoose.model('Doctor', doctorShema)