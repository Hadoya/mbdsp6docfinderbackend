const express = require('express')
const cors = require('cors')
const mongoose=require('mongoose')

require('dotenv').config();

const app = express()
const port = process.env.PORT || 5000
const bodyParser = require('body-parser')

app.use(cors())
app.use(express.json()) //here

const uri=process.env.ATLAS_URI;
mongoose.connect(uri,{useNewUrlParser:true,useCreateIndex:true}
);

app.use(bodyParser.urlencoded({ extended: false }))

const connection=mongoose.connection;
connection.once('open',()=>{
    console.log("MongoDB database connection established successfully")
})

const exerciceRouter=require('./routes/exercice')
const userRouter=require('./routes/user')
const doctorRouter=require('./routes/doctor')
const leSaviezVousRouter=require('./routes/leSaviezVous')
const leSaviezVousCategoryRouter=require('./routes/leSaviezVousCategory')
const patientRouter=require('./routes/patient')
const patientDetailsRouter=require('./routes/patientDetails')
const symptomRouter=require('./routes/symptom')
const appointmentRouter=require('./routes/appointment')
const appointmentTypeRouter=require('./routes/appointmentType')
const consultationRouter=require('./routes/consultation')
const prescriptionRouter=require('./routes/prescription')
const symptomPerDiseaseRouter=require('./routes/symptomPerDisease')
const diseaseRouter=require('./routes/disease')
const specialityRouter=require('./routes/doctor')
const doctorSpecialityRouter=require('./routes/doctorSpeciality')
const userAdminRouter=require('./routes/userAdmin')

app.use('/exercice',exerciceRouter)
app.use('/user',userRouter)
app.use('/doctor',doctorRouter)

app.use('/patient',patientRouter)
app.use('/patientDetails',patientDetailsRouter)
app.use('/symptom',symptomRouter)
app.use('/appointment',appointmentRouter)
app.use('/appointmentType',appointmentTypeRouter)

app.use('/consultation',consultationRouter)
app.use('/prescription',prescriptionRouter)
app.use('/symptomPerDisease',symptomPerDiseaseRouter)
app.use('/disease',diseaseRouter)
app.use('/speciality',specialityRouter)
app.use('/doctorSpeciality',doctorSpecialityRouter)
app.use('/leSaviezVous',leSaviezVousRouter)
app.use('/leSaviezVousCategory',leSaviezVousCategoryRouter)
app.use('/userAdmin',userAdminRouter)

app.get('/', (req, res) => res.status(200).send({
  message: 'Welcome to the backend of the docfinder app',
}));

app.listen(port, () => {
  
  console.log(`Server running on port ${port}`);
});